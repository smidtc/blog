---
title: About this blog
subtitle: posting on research and michigan politics
comments: false
---

This blog serves to post updates on what I have done, what I am working on, and what I find amusing. I moving back to a blog format since I have had a general dissatisfaction with how academic posting has devolved on Twitter.

Things you will see posted here

- Updates on research projects
- Data analysis and updates on Michigan politics
- Conference presentations
- Any other random posts that use data.

## About me?

If you are looking for information about me, my [primary website](https://smidtc.gitlab.io) covers what you need to know. 

