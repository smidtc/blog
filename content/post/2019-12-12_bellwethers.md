---
title: Let's define a better bellwether - with a focus on Michigan
date: 2019-12-12
author: Corwin D. Smidt
tags: ["Michigan","swingvoters"]
---

-   [A county worth chronicling?](#a-county-worth-chronicling)
    -   [Rethinking Bellwethers](#rethinking-bellwethers)
-   [In search of two key qualities](#in-search-of-two-key-qualities)
    -   [Volatile counties as bellwethers](#volatile-counties-as-bellwethers)
    -   [Indicative counties as bellwethers](#indicative-counties-as-bellwethers)
-   [Combining these two qualities: the Swing Multiplier](#combining-these-two-qualities-the-swing-multiplier)
    -   [Defining the Swing Multiplier](#defining-the-swing-multiplier)
    -   [Swing Multiplier results](#swing-multiplier-results)
-   [Crowning Michigan's bellwether counties](#crowning-michigan-s-bellwether-counties)
    -   [1st Cass](#1st-cass)
    -   [2nd Wexford](#2nd-wexford)
    -   [3rd Luce](#3rd-luce)
    -   [Not Washtenaw!](#not-washtenaw)
-   [General thoughts](#general-thoughts)
-   [Coda: Michigan demographics don't have a clear destiny.](#coda-michigan-demographics-dont-have-a-clear-destiny.)

**Summary**: Data-driven discussion and identification of what should be Michigan's bellwether counties point to a consistent set of counties that exhibit sizable changes each election that are also accurate reflections of statewide trends. A county's propensity to both change and change in the same way as the state is negatively correlated with its percentage of college graduates.

A county worth chronicling?
===========================

NBC News [recently named Kent County](https://www.woodtv.com/news/kent-county/nbc-news-will-zero-in-on-kent-county-for-2020-election/) as one of the five counties it will be closely watching before the 2020 Election.

That's understandable in that Kent is a growing, large, purple county that is experiencing a lot of demographic changes. And it has an interesting House race to boot. Justin Amash is now running as an independent and there are some compelling candidates on the Democrat and Republican sides running too (my way too early call is that the Democrat Hillary Scholten takes it). Kent also reflects the national trend of the rich suburbs trending toward Democrats. And the affluent Oakland and Kent counties played a strong part in Governor Whitmer's win in 2018. These Romney Republican counties had relatively high third party support in 2016, and if those votes swing to Democrats that is a lot of votes.

Kent is a battleground county; small movements in it are very important because it is populous and evenly split.

But battleground counties don't really tell us much about where an election is going. It is hard to decipher meaningful trends in these counties because they are so populous and divided. They provide few telling indications of what's driving Michigan voters because:

1.  They are large counties that have saturated media markets with a lot of re-enforcing messages that limit persuasion. Swings will be less about persuasion and more about compositional changes or mobilization.
2.  They have a largely educated and wealthier voter base, those voters on both sides are more stuck in their ways. What makes it a battle is that it is over decided turning voters out.
3.  In Kent's case, much of the changes in its support is not a result of voter change, but an influx of urban liberal voters coming to live in and around Grand Rapids.

Rethinking Bellwethers
----------------------

So what are better counties or regions to cover? And how can we identify those more concretely?

Battlegrounds matter for campaign strategist, but if I was a journalist then I think a focus on **bellwether** counties makes more sense. Bellwethers aren't traditionally embraced by political science. But that is because the concept is traditionally measured in very poor way. Counties are termed bellwethers based on a history of coincidence wherein a county has picked the winner of the state or country for the last X elections and what defines a successful county pick is a very arbitrary 50% cutoff.

But there is something cool about the concept of a bellwether. I would greatly appreciate campaign coverage of a county that both: 1) had a history of substantial changes in its party support from election to election such that campaign coverage could decipher how it was changing; and 2) demonstrated a history of changes that consistently track with the changes observed at the state level. Getting a sense of how voters in those counties are viewing the election would likely be revealing for the entire state. Politics can change, but such a process of identification would still provide a better chance of understanding changes in the Michigan electorate than simply choosing split counties.

In search of two key qualities
==============================

So how could this be done? First a disclaimer: I am finding bellwethers by looking at election trends since 1996. This means limited data (only 6 elections). Going further in the past would address this issue, but I don't think that information is closely connected to our current party system. A post-NAFTA analysis that ignores Perot's 1992 popularity and tracks Michigan politics with the decline of manufacturing made most sense to me. All estimates I present are bootstrapped bias-corrected estimates. Frankly, this doesn't help much, but it is about all I can do.

I am going to focus on measuring an identifying bellwethers that represent two key qualities:

1.  **Volatile counties**, those that provide the clearest signal in that they change the most from election to election
2.  **Indicative counties**, those that are more like a microcosm of the state in that they are closely associated with statewide trends and results.

Then I'm going to combine these two measures into a single bellwether county measure of that represents counties that are both the most volatile in the state and the most reflective of statewide trends. These would be the counties I consider to be top bellwethers, and counties I would like to see some coverage by journalists. These counties send both the strongest signal of change and their change has a history of being a good reflection of state changes

(**For less data minded reader, this is a good point to skip on down to the maps with [Swing Multiplier results](#swing-multiplier-results) or [Crowning Michigan's bellwether counties](#crowning-michigan-s-bellwether-counties).**)

Volatile counties as bellwethers
--------------------------------

Let's compare the trends in presidential voting from the battleground counties of Oakland and Kent with two smaller counties, one in the Upper Peninsula near the Wisconsin border and one in the Lower Peninsula near the Indiana border, respectively Ontonagon and Cass.

![](figure-markdown_github/swing-1.png)

Kent is more Republican, but the two battleground counties trend very closely together (r=.95). Moreover, outside of 2008, the changes in Kent and Oakland are mostly small and incremental.

The smaller counties of Cass and Ontonagon don't completely track with the first two; these counties trended toward the GOP in 2016, whereas Kent and Oakland trended slightly away. But the volatile counties aren't necessary better reflections of the state. Ontonagon became less supportive of Bush in 2004, although the state as a whole became more supportive.

The point of this illustration is not the trend, but the size of the change. Both Ontonagon and Cass exhibit much larger changes in their two-party support from election to election than either Kent or Oakland. All counties went more Republican in 2000, but Ontonagon went from being the most supportive of the Democrats in 1996 to the least supportive in 2000 among the four. From election to election, either Cass or Ontonagon exhibit the largest change and are the most volatile.

### Identifying volatile counties

Now an easy way to identify volatile counties is to find the counties with the largest average change in two party support from election to election (the mean of the absolute difference in two party voting).[1] I prefer the size of the difference over a standard deviation measure because variances also grow as a mean becomes a worse measure of a county's central tendency (i.e, variances make trending counties look as volatile as swing counties).

The following map of average changes in proportions highlights the most volatile counties in the state.

![](figure-markdown_github/change-1.png)

And here are the eight counties that change the most from election to election, along with the number of voters in that county in 2016. These are displayed as proportions, thus Ontonagon averages a 10 percentage point change in its two party vote from election to election.

| County                 |  Average Change|  Votes in 2016|
|:-----------------------|---------------:|--------------:|
| Ontonagon County, MI   |       0.0983514|           3420|
| Luce County, MI        |       0.0796398|           2576|
| Menominee County, MI   |       0.0794676|          10765|
| Montmorency County, MI |       0.0789199|           5008|
| Lake County, MI        |       0.0787296|           5328|
| Kalkaska County, MI    |       0.0774740|           8821|
| Cass County, MI        |       0.0765307|          23272|
| Wexford County, MI     |       0.0752080|          15279|

These are all fairly small counties. But that doesn't seem to be a direct cause of their high levels of volatility.[2] Ontonagon stands out as uniquely volatile, averaging a near 2 percentage point greater swing each election than other counties. Ontonagon's volatility likely represents demographic trends since it recently saw a large decline in its population from 2008-2016. But population changes aren't a contributing factor for the rest. They also represent different regions of the state, with Cass in southern Michigan and Kalkaska, Lake, Montmorency and Wexford in the northern Lower Peninsula and Luce, Menominee, and Ontonagon in the Upper Peninsula.

While race certainly plays a part in Wayne for its stability, the education profile of a state is clearly a factor in distinguishing volatile counties from now volatile counties. College education is obviously associated with a host of other economic and social factors, but the simple scatterplot makes clear that volatility is much lower in counties with a larger percentage of adults with college degrees.

![](figure-markdown_github/changeeduc-1.png)

Indicative counties as bellwethers
----------------------------------

These volatile counties exhibit sharp changes and appear to be more responsive to short term forces, but their patterns don't always trace well with trends observed at the state level. For this quality we are looking for counties that are good indicators of statewide trends.

![](figure-markdown_github/corr-1.png)

The figure above plots the State of Michigan's support for the Democratic party across each Election and compares it to trends in Benzie, Kent, St. Joseph, and Washtenaw county. These counties vary in how well they reflect the trends for the whole state. Although they move with the state in all but two elections, both Washtenaw and Kent are trending in the opposite direction of the state overall. The distance between Washtenaw's purple line and the state's black line gets farther apart each election. The distance between Kent and the average are also progressively getting closer. In contrast, Benzie and St. Joseph track the state in a very proportional manner. Their level of support differs from the state, but their trends are highly correlated.

![](figure-markdown_github/corrmap-1.png)

The map above plots out the correlation of county and statewide trends and reveals some stark contrasts. What's striking is not which counties correlate well with the state; most of the county trends range from nearly strong to near perfect linear associations with statewide trends. **But what is striking is that there are seven prominent counties that have a very poor association with the rest of the state**. Starting with the lowest, there seven counties in Michigan that correlate below .5 with statewide trends: **Washtenaw, Kent, Ingham, Leelanau, Kalamazoo, Ottawa, and Oakland.** Perhaps then it is not a good idea for NBC to pick Kent as a microcosm of the political currents in Michigan.

For the non-Michiganders, let's make the substance of that list a little clearer. ![](figure-markdown_github/correduc-1.png)

So if a county is over 30% college educated, its a good bet that its trends are not reflective of recent trends in Michigan. Perhaps not surprisingly, a good guide for choosing a indicative county of Michigan politics is to focus on a county has an education profile similar to the whole of the state.

### Identifying indicative counties

As mentioned above, a problem with focusing on trends in the average is that it is based on variances relative to the average over 20 years. And that average for a county may not be stable nor the best baseline for assessing what counties change in a manner indicative of the state. In short, I want to find the counties that track well with **changes** in Michigan's vote, not just those that track well in their levels of two-party support. Along those lines, I'm focusing on a county's correlation with the state in election-to-election changes. As shown below, the map of county changes that strongly correlate with state changes look very similar.

![](figure-markdown_github/changemap-1.png)

And to re-emphasize the validity of this correlation, note that the correlation between county and statewide changes has a stronger linear association with the education level of a county. The counties highest in educational attainment are the least likely to change in the same direction as the whole state.

![](figure-markdown_github/scatterchangecorr-1.png)

For continuity sake, I list the top 8 counties (below) with changes that correlate with statewide changes below, but note that many counties have strong associations. The top eight is again diverse in region and profile. If you couldn't tell from the graph above, there is not much that separates the counties at the top, so it is unclear how much to focus on one as the best. For what it is worth, Isabella is not only a top correlate with statewide changes, but it also the county that is typically the closest to the statewide average each year. Also, note that Wayne County may not be that volatile nor representative of the state, but since it is so large its changes remain strongly indicative of state-level changes.

| County                |  Correlation|  Votes in 2016|
|:----------------------|------------:|--------------:|
| Allegan County, MI    |    0.9929100|          55658|
| Isabella County, MI   |    0.9903482|          25344|
| Mason County, MI      |    0.9902896|          14658|
| Otsego County, MI     |    0.9882898|          12529|
| Van Buren County, MI  |    0.9877539|          33213|
| Barry County, MI      |    0.9872905|          30265|
| Wexford County, MI    |    0.9815923|          15279|
| St. Joseph County, MI |    0.9790133|          23765|

Combining these two qualities: the Swing Multiplier
===================================================

If a bellwether county is the county that is both a strong signal of change and one that changes in step with the state, then it should be both high in changes and its changes should be highly correlated with state changes.

At this point I am tempted to just take the product of these two measures (Average Change and County Change Correlation with the State Change). But a product-based measure of these two summary measures doesn't have a clear interpretation, it is based on aggregates, and it would be a little dominated by the scale of the volatility group.

Defining the Swing Multiplier
-----------------------------

So let's come up with a better measure. I want to propose the **swing multiplier** as that measure. This measure compares the direction and size of change in the two-party vote at the county level and compares it to the direction and size of change for the state via a ratio for each election.

![ 
SwingMultiplier\_t = \\frac{CountyVote\_t - CountyVote\_{t-1}}{StateVote\_t - StateVote\_{t-1}} 
](https://latex.codecogs.com/png.latex?%20%0ASwingMultiplier_t%20%3D%20%5Cfrac%7BCountyVote_t%20-%20CountyVote_%7Bt-1%7D%7D%7BStateVote_t%20-%20StateVote_%7Bt-1%7D%7D%20%0A " 
SwingMultiplier_t = \frac{CountyVote_t - CountyVote_{t-1}}{StateVote_t - StateVote_{t-1}} 
")

If the swing multiplier is above 1, that means a county's vote changed in the same direction of the state and the size of that change was greater than the statewide change. Negative values indicate a county's vote changed in a direction opposite to the statewide trend.

Ratio based measures are usually bad form from a sampling perspective since the ratio of two percentage differences can be extremely noisy. For instance, when the statewide margin of change is really small, the magnitude of the multiplier value for that election will be larger than others. So I would not advocate identifying bellwethers by the largest average, that choice should rely on multiple robust approaches for identifying counties that are at the top in bellwether status. I present two of these approaches (highest median swing multiplier, average of each election's standardized swing multiplier, average county rank in swing multiplier) as well as how those measures compare to ranking based on the average swing multiplier and the product of the two measures presented above.

The median swing multiplier (SM), average of standardized SM, and mean rank SM measures track extremely closely together and with the product of the two constituent terms (correlation strengths range from .85 to .9 with the product measure, and these are more robust). Since the median swing multiplier has the most straightforward interpretation, I will start with that one first in the following discussion.

Swing Multiplier results
------------------------

I have tabbed the sections to allow for a quick comparison across all three approaches. Regardless of the county ranking by each measure, a comparison of the maps shows that a consistent set of regions are identified as bellwethers and not bellwethers.

### Median SM

![](figure-markdown_github/medswing-1.png)

Taking the median estimate suggests that the counties listed below typically exhibit a swing in the same direction of the state, **but at twice the proportional rate**.

| County                 |  Med. Swing Mult.|  Votes in 2016|
|:-----------------------|-----------------:|--------------:|
| Lake County, MI        |          2.324027|           5328|
| Montmorency County, MI |          2.253104|           5008|
| Kalkaska County, MI    |          2.144074|           8821|
| Wexford County, MI     |          2.041417|          15279|
| Cass County, MI        |          2.027455|          23272|
| Ogemaw County, MI      |          1.967201|          10386|
| Luce County, MI        |          1.964090|           2576|
| Branch County, MI      |          1.938371|          17635|

### Avg. Std. SM

![](figure-markdown_github/stdmap-1.png)

The standardizing the swing multiplier by election we see that Cass County typically observes a swing multiplier that is one standard deviation larger than average for other counties in Michigan.

| County                 |  Mean of Std. S|  Votes in 2016|
|:-----------------------|---------------:|--------------:|
| Cass County, MI        |       1.1445260|          23272|
| Luce County, MI        |       0.8145862|           2576|
| Kalkaska County, MI    |       0.7576685|           8821|
| Branch County, MI      |       0.7237083|          17635|
| Montmorency County, MI |       0.7046004|           5008|
| Wexford County, MI     |       0.6897256|          15279|
| Missaukee County, MI   |       0.6558099|           7301|
| Lake County, MI        |       0.6487033|           5328|

### Average Rank of Swing Multiplier

![](figure-markdown_github/meanrankswing-1.png)

By taking the average of each election's rank order of the swing multiplier, we see that a county like Wexford County is the only county to average a ranking in the top quartile of county swing.

| County                 |  Average Rank|  Votes in 2016|
|:-----------------------|-------------:|--------------:|
| Wexford County, MI     |      19.32088|          15279|
| Luce County, MI        |      22.83792|           2576|
| Lake County, MI        |      24.03150|           5328|
| Cass County, MI        |      24.72946|          23272|
| Branch County, MI      |      25.41512|          17635|
| Missaukee County, MI   |      25.59200|           7301|
| Newaygo County, MI     |      26.67971|          22605|
| Montmorency County, MI |      26.81963|           5008|

Crowning Michigan's bellwether counties
=======================================

Reviewing across the three maps, Cass consistently appears as Michigan's top bellwether, and the second and third place counties are not far behind and represent the other prominent bellwether regions of Michigan. Let's summarize each case:

![](figure-markdown_github/top5-1.png)

1st Cass
--------

[Cass County](https://en.wikipedia.org/wiki/Cass_County,_Michigan) consistently ranks in the top five by each ranking approach. It is also one of more populous counties at the top and noteworthy for having a high average swing without dramatically sacrificing correlation with the state in terms of both swing and trend. Neighboring Michiana area counties Branch and Saint Joseph don't make the top five, but consistently rank as top bellwethers too.

**Average swing:** 7.65 percentage points

**Change in Vote Correlation with State Change in Vote:** 0.89

**Vote Trend Correlation with State Trend:** 0.90

|  Election|  Votes|  Major Party % (Dem)|  State Major Party|  Swing Multiplier|
|---------:|------:|--------------------:|------------------:|-----------------:|
|      1996|  17980|            0.5267651|          0.5732442|                NA|
|      2000|  19825|            0.4551232|          0.5263461|          1.527608|
|      2004|  22697|            0.4238478|          0.5172585|          3.441541|
|      2008|  23576|            0.5208863|          0.5837130|          1.460224|
|      2012|  22395|            0.4310562|          0.5480053|          2.515711|
|      2016|  23272|            0.3379666|          0.4985574|          1.882575|

2nd Wexford
-----------

[Wexford County](https://en.wikipedia.org/wiki/Wexford_County,_Michigan) swings slightly less than Cass, but it stands out for having big swings that still strongly correlate with proportion of swings observed in the state vote. Wexford is also represents the strongest bellwether region, as many interior northern lower Michigan counties (Lake, Montmorency, and Kalkaska) rank in the top 10 in terms of bellwether tendency.

**Average swing:** 7.52 percentage points

**Change in Vote Correlation with State Change in Vote:** 0.98

**Vote Trend Correlation with State Trend:** 0.89

|  Election|  Votes|  Major Party % (Dem)|  State Major Party|  Swing Multiplier|
|---------:|------:|--------------------:|------------------:|-----------------:|
|      1996|  11888|            0.5310332|          0.5732442|                NA|
|      2000|  12982|            0.4246870|          0.5263461|          2.267602|
|      2004|  15160|            0.4022667|          0.5172585|          2.467130|
|      2008|  15704|            0.4784413|          0.5837130|          1.146267|
|      2012|  14781|            0.4225776|          0.5480053|          1.564475|
|      2016|  15279|            0.3072873|          0.4985574|          2.331546|

3rd Luce
--------

[Luce County](https://en.wikipedia.org/wiki/Luce_County,_Michigan) is one of the least populous counties in the state and the second most volatile. It is the top county from the Upper Peninsula bellwether region. Luce tends to be slightly redder than other UP bellwether counties that swung to Obama in 2008. But a focus on changes (not on levels) points to it having the largest swings that accurately reflect changes across Michigan.

**Average swing:** 7.96 percentage points

**Change in Vote Correlation with State Change in Vote:** 0.93

**Vote Trend Correlation with State Trend:** 0.86

|  Election|  Votes|  Major Party % (Dem)|  State Major Party|  Swing Multiplier|
|---------:|------:|--------------------:|------------------:|-----------------:|
|      1996|   2459|            0.5345244|          0.5732442|                NA|
|      2000|   2536|            0.3924466|          0.5263461|          3.029501|
|      2004|   2829|            0.3740157|          0.5172585|          2.028131|
|      2008|   2740|            0.4442372|          0.5837130|          1.056685|
|      2012|   2590|            0.3854531|          0.5480053|          1.646260|
|      2016|   2576|            0.2794419|          0.4985574|          2.143893|

Not Washtenaw!
--------------

[Washtenaw County](https://en.wikipedia.org/wiki/Washtenaw_County,_Michigan) is tops in educational attainment but gets the crown for the absolute worst bellwether county in Michigan; it ranks dead last across all three approaches. If it moves like the state, it does so at a far smaller rate. Its bootstrapped median swing multiplier is slightly negative and it averages a rank of 68 out of the 83 counties.

**Average swing:** 3.37 percentage points

**Change in Vote Correlation with State Change in Vote:** 0.56

**Vote Trend Correlation with State Trend:** 0.08

|  Election|   Votes|  Major Party % (Dem)|  State Major Party|  Swing Multiplier|
|---------:|-------:|--------------------:|------------------:|-----------------:|
|      1996|  124028|            0.6457956|          0.5732442|                NA|
|      2000|  144940|            0.6228847|          0.5263461|         0.4885253|
|      2004|  173264|            0.6414695|          0.5172585|        -2.0450661|
|      2008|  187115|            0.7076478|          0.5837130|         0.9958436|
|      2012|  179233|            0.6818310|          0.5480053|         0.7230046|
|      2016|  187903|            0.7172932|          0.4985574|        -0.7171617|

General thoughts
================

There is obviously a small sample bias in this study. It is unclear if counties lower in educational attainment are always more volatile, or just happen to be the group that is without a place in the 1996-2016 party system. There has been a lot written about white working class voters since 2016. Not all of it agrees, but much of it demonstrates they are a group that haven't had a strong home in either party, but they have been attracted to the GOP because of a mixture of either social conservativism, racial animosity, and economic populism.

With that disclaimer in mind, Michigan provides little evidence that upper class whites change their party support at the same rates as lower class whites. Such regions change but at a much slower rate. Kent and Oakland are traditional bases of the Michigan GOP, both county's affluence has attracted many educated liberals over the last 10-20 years and both have become more supporting of Democrats. But neither county managed to support Clinton in 2016 at the same level it supported Obama in 2008. In contrast, Cass swung its vote by over 15 percentage points.

If we want to know how a Biden nomination in Michigan might do compared a Warren nomination, or how Michigan voters might react to Trump's trade policies, then a discussion with voters in places like Cass and Wexford makes the most sense.

Coda: Michigan demographics don't have a clear destiny.
=======================================================

The notion in politics that demographics is destiny is a bit oversold, but there is some meaning to it. And it is fair to question whether these bellwether counties will matter, since some prominent in-depth [political analyses](https://www.freep.com/story/news/local/michigan/2019/05/23/explained-michigan-politics-trump/3694443002/) of [demographic trends](https://www.bridgemi.com/michigan-government/gerrymandering-dying-michigan-old-age-no-joke) in Michigan make it seem like Michigan is trending blue.

My own take is that these summaries seem to be picking and choosing trends to extrapolate. For instance, let's look at the top ten counties (in terms of votes) and their relative size in Michigan's presidential elections. ![](figure-markdown_github/countytrends-1.png)

Although the counties gaining population are becoming less Republican than they were before, the counties losing population are some of the bluest in the state.

So which party is favored by recent demographic trends? Use a county's average percentage voting Democratic of the two-party vote from 2012 and 2016 as a measure of county partisan support (county normal vote). By keeping party support for each county constant at that average, the influence of demographic trends can be found by totaling up state support by using the observed proportion of voters from the county in each calculate the state vote by re-weighting base the proportion voters in the county. Since party support in the county is held constant, county proportion of the electorate is the only source of change are the party support across these years, the let's calculate demographic trends by county support using the share of the presidential and gubernatorial electorate since 1996.

![](figure-markdown_github/unnamed-chunk-1-1.svg)

Michigan was at its most Republican in 2010, but things aren't clearly trending up. Indeed, 2014-2018 look like a very similar landscape. Beyond 2012, turnout differences in blue vs. red counties have made no net contribution to each party's recent electoral performance. depending on how you look at it, either 2016 or 2018 was remarkable in that the general and midterm electorates are indistinguishable in partisan bent.

In summary, **there has been no net contribution of demographic shifts or turnout differences on the statewide vote across the years 2014, 2016, and 2018**.

\-CDS

[1] This is essentially the same as the multi-party [Pedersen Index](https://en.wikipedia.org/wiki/Pedersen_index) measure of volatility when there are only two parties.

[2] If you control for size of county electorate in the scale parameter of a beta regression with a lagged DV and county dummies in the scale, then the results point to even smaller counties as more volatile. If one controls for turnout then that sort of lessens the concept of what makes a county a swing county.
